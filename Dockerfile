FROM jpmml/openscoring_vanila:latest

ADD application.conf /opt/openscoring/application.conf

EXPOSE 8080

ENTRYPOINT java -Dconfig.file=/opt/openscoring/application.conf -jar /opt/openscoring/openscoring-server/target/server-executable-1.3-SNAPSHOT.jar
