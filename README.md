Openscoring application for the Docker distributed applications platform

# Overview #

[Openscoring] (https://gitlab.com/avinandan/openscoring) provides REST API for publishing and evaluating predictive models:

* Model deployment and undeployment
* Model evaluation in single prediction, batch prediction and CSV prediction modes
* Model metrics

# Installation #

Prerequisites:

* Docker 1.5 or newer

GitLab repository [openscoring] (https://gitlab.com/avinandan/openscoring) contains a `Dockerfile` for Openscoring command-line server application.

Building the `latest` Openscoring application image from the `HEAD` revision:

```
git clone https://lab.dietco.de/dc/openscoring
cd openscoring
sudo docker build -t openscoring:latest .
```

# Usage #

### Interactive mode ###

Running the image in the `host` networking mode:

```
sudo docker run --net=<network-name> openscoring:latest
```

The container shares host's network stack. It is possible to use privileged HTTP methods `PUT` and `DELETE` for deploying and undeploying models, respectively.

### Passive mode ###

Running the image in the `bridge` (default) networking mode:

```
sudo docker run --net=<network-name> -p 8080:8080 -v /path/to/pmml:/openscoring/pmml openscoring:latest --model-dir /openscoring/pmml
```

The container uses Docker's default network setup, which is separate from host's network stack. It is impossible to use privileged HTTP methods. The only option for deploying and undeploying models is via the model auto-deployment directory `/openscoring/pmml`. This directory is mapped to host's filesystem directory `/path/to/pmml` using the *data volume* mechanism.

